# DesafioPb

Site pequeno para cadastro de Cervejas, api JSON, utilizando:
------------------------------------
* Elixir
* Phoenix
* PostgreSQL
* GIT(Version Control)
* ATOM(IDE)


Para iniciar o site em Phoenix:

1. Install dependencies with `mix deps.get`
2. Start Phoenix endpoint with `mix phoenix.server`

Pode entrar no site [`localhost:4000`](http://localhost:4000) no teu browser.
