defmodule DesafioPb.Repo.Migrations.CreateTipo do
  use Ecto.Migration

  def change do
    create table(:tipos) do
      add :nome, :string

      timestamps
    end

  end
end
