defmodule DesafioPb.Repo.Migrations.CreateCerveja do
  use Ecto.Migration

  def change do
    create table(:cervejas) do
      add :nome, :string
      add :data_criacao, :date
      add :ingredientes, :string
      add :tipo, :string
      add :volume, :integer

      timestamps
    end

  end
end
