defmodule DesafioPb.TipoTest do
  use DesafioPb.ModelCase

  alias DesafioPb.Tipo

  @valid_attrs %{nome: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Tipo.changeset(%Tipo{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Tipo.changeset(%Tipo{}, @invalid_attrs)
    refute changeset.valid?
  end
end
