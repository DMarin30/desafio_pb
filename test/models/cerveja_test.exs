defmodule DesafioPb.CervejaTest do
  use DesafioPb.ModelCase

  alias DesafioPb.Cerveja

  @valid_attrs %{data_criacao: %{day: 17, month: 4, year: 2010}, ingredientes: "some content", nome: "some content", tipo: "some content", volume: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Cerveja.changeset(%Cerveja{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Cerveja.changeset(%Cerveja{}, @invalid_attrs)
    refute changeset.valid?
  end
end
