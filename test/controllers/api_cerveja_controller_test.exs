defmodule DesafioPb.APICervejaControllerTest do
  use DesafioPb.ConnCase

  alias DesafioPb.Cerveja
  @valid_attrs %{data_criacao: %{day: 17, month: 4, year: 2010}, ingredientes: "some content", nome: "some content", tipo: "some content", volume: 42}
  @invalid_attrs %{}

  setup do
    conn = conn() |> put_req_header("accept", "application/json")
    {:ok, conn: conn}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, api_cerveja_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end
end
