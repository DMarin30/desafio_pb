defmodule DesafioPb.CervejaControllerTest do
  use DesafioPb.ConnCase

  alias DesafioPb.Cerveja
  @valid_attrs %{data_criacao: %{day: 17, month: 4, year: 2010}, ingredientes: "some content", nome: "some content", tipo: "some content", volume: 42}
  @invalid_attrs %{}

  setup do
    conn = conn()
    {:ok, conn: conn}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, cerveja_path(conn, :index)
    assert html_response(conn, 200) =~ "Lista das cervejas"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, cerveja_path(conn, :new)
    assert html_response(conn, 200) =~ "Nova cerveja"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, cerveja_path(conn, :create), cerveja: @valid_attrs
    assert redirected_to(conn) == cerveja_path(conn, :index)
    assert Repo.get_by(Cerveja, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, cerveja_path(conn, :create), cerveja: @invalid_attrs
    assert html_response(conn, 200) =~ "Nova cerveja"
  end

  test "shows chosen resource", %{conn: conn} do
    cerveja = Repo.insert! %Cerveja{}
    conn = get conn, cerveja_path(conn, :show, cerveja)
    assert html_response(conn, 200) =~ "Detalhe da cerveja"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_raise Ecto.NoResultsError, fn ->
      get conn, cerveja_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    cerveja = Repo.insert! %Cerveja{}
    conn = get conn, cerveja_path(conn, :edit, cerveja)
    assert html_response(conn, 200) =~ "Edit cerveja"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    cerveja = Repo.insert! %Cerveja{}
    conn = put conn, cerveja_path(conn, :update, cerveja), cerveja: @valid_attrs
    assert redirected_to(conn) == cerveja_path(conn, :show, cerveja)
    assert Repo.get_by(Cerveja, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    cerveja = Repo.insert! %Cerveja{}
    conn = put conn, cerveja_path(conn, :update, cerveja), cerveja: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit cerveja"
  end

  test "deletes chosen resource", %{conn: conn} do
    cerveja = Repo.insert! %Cerveja{}
    conn = delete conn, cerveja_path(conn, :delete, cerveja)
    assert redirected_to(conn) == cerveja_path(conn, :index)
    refute Repo.get(Cerveja, cerveja.id)
  end
  ##Testes feitos Manualmente para o API


  ##Fim dos teste manuais
end
