defmodule DesafioPb.PageControllerTest do
  use DesafioPb.ConnCase

  test "GET /" do
    conn = get conn(), "/"
    assert html_response(conn, 200) =~ "Cervejaria Phoenix!"
  end
end
