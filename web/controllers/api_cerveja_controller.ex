defmodule DesafioPb.APICervejaController do
  use DesafioPb.Web, :controller

  alias DesafioPb.Cerveja

  plug :scrub_params, "cerveja" when action in [:create, :update]

  def index(conn, _params) do
    cervejas = Repo.all(Cerveja)
    render(conn, "index.json", cervejas: cervejas)
  end
end
