defmodule DesafioPb.CervejaController do
  use DesafioPb.Web, :controller

  alias DesafioPb.Cerveja

  plug :scrub_params, "cerveja" when action in [:create, :update]

  def index(conn, _params) do
    cervejas = Repo.all(Cerveja)
    render(conn, "index.html", cervejas: cervejas)
  end

  def apilist(conn, _params) do
    cervejas = Repo.all(Cerveja)
    conn
    |> put_status(201)
    |> json  %{ok: true, cervejas: cervejas}
  end

  def new(conn, _params) do
    changeset = Cerveja.changeset(%Cerveja{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"cerveja" => cerveja_params}) do
    changeset = Cerveja.changeset(%Cerveja{}, cerveja_params)

    case Repo.insert(changeset) do
      {:ok, _cerveja} ->
        conn
        |> put_flash(:info, "Cerveja criada.")
        |> redirect(to: cerveja_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    cerveja = Repo.get!(Cerveja, id)
    render(conn, "show.html", cerveja: cerveja)
  end

  def edit(conn, %{"id" => id}) do
    cerveja = Repo.get!(Cerveja, id)
    changeset = Cerveja.changeset(cerveja)
    render(conn, "edit.html", cerveja: cerveja, changeset: changeset)
  end

  def update(conn, %{"id" => id, "cerveja" => cerveja_params}) do
    cerveja = Repo.get!(Cerveja, id)
    changeset = Cerveja.changeset(cerveja, cerveja_params)

    case Repo.update(changeset) do
      {:ok, cerveja} ->
        conn
        |> put_flash(:info, "Cerveja foi modificada.")
        |> redirect(to: cerveja_path(conn, :show, cerveja))
      {:error, changeset} ->
        render(conn, "edit.html", cerveja: cerveja, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    cerveja = Repo.get!(Cerveja, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(cerveja)

    conn
    |> put_flash(:info, "Cerveja apagada.")
    |> redirect(to: cerveja_path(conn, :index))
  end
end
