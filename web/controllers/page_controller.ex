defmodule DesafioPb.PageController do
  use DesafioPb.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
