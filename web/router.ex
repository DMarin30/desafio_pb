defmodule DesafioPb.Router do
  use DesafioPb.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", DesafioPb do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    #get "/list", CervejaController, :index
    #get "/edit/:id", CervejaController, :edit
    resources "/cerveja", CervejaController
  end

  # Other scopes may use custom stacks.
  scope "/api", DesafioPb do
     pipe_through :api
     get "/cerveja", APICervejaController, :index
  end
end
