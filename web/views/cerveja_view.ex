defmodule DesafioPb.CervejaView do
  use DesafioPb.Web, :view

  def render("index.json", %{cervejas: cervejas}) do
    %{data: render_many(cervejas, "cerveja.json")}
  end

  def render("show.json", %{cerveja: cerveja}) do
    %{data: render_one(cerveja, "cerveja.json")}
  end

  def render("cerveja.json", %{cerveja: cerveja}) do
    %{id: cerveja.id, nome: cerveja.nome, data_criacao: cerveja.data_criacao, ingredientes: cerveja.ingredientes, tipo: cerveja.tipo, volume: cerveja.volume }
  end
end
