defmodule DesafioPb.Cerveja do
  use DesafioPb.Web, :model

  schema "cervejas" do
    field :nome, :string
    field :data_criacao, Ecto.Date
    field :ingredientes, :string
    field :tipo, :string
    field :volume, :integer

    timestamps
  end

  @required_fields ~w(nome data_criacao ingredientes tipo volume)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
